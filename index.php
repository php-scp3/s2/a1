<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>

	<!-- [ACTIVITY 1] -->
	<h1>Divisibles of Five</h1>
	<?= number(); ?>


	<!-- [ACTIVITY 2] -->
	<h1>Array Manipulation</h1>
		<?php array_push($students, 'Jane'); ?>
		<pre><?php var_dump($students); ?></pre>
		<p><?= count($students); ?></p>

		<?php array_unshift($students, 'John'); ?>
		<pre><?php var_dump($students); ?></pre>
		<p><?= count($students); ?></p>

		<?php array_shift($students); ?>
		<pre><?php var_dump($students); ?></pre>
		<p><?= count($students); ?></p>

</body>
</html>